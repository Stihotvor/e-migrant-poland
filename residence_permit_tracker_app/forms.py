from django import forms

from residence_permit_tracker_app.models import ResidencePermitProcessHistory


class ResidencePermitProcessHistoryRecordCreateForm(forms.ModelForm):
    class Meta:
        model = ResidencePermitProcessHistory
        fields = ['residence_permit_process', 'date', 'activity_type', 'details']
        widgets = {'residence_permit_process': forms.HiddenInput()}



