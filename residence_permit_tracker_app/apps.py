from django.apps import AppConfig


class ResidencePermitTrackerAppConfig(AppConfig):
    name = 'residence_permit_tracker_app'
    verbose_name = 'Residence documents process tracker'
