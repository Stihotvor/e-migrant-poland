from django.contrib import admin

from residence_permit_tracker_app.models import ResidencePermitType, ResidencePermitStatus, ResidencePermitProcess, \
    ProcessActivityType, ResidencePermitProcessHistory


class ResidencePermitTypeAdmin(admin.ModelAdmin):
    pass


class ResidencePermitStatusAdmin(admin.ModelAdmin):
    pass


class ResidencePermitProcessAdmin(admin.ModelAdmin):
    pass


class ProcessActivityTypeAdmin(admin.ModelAdmin):
    pass


class ResidencePermitProcessHistoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(ResidencePermitType, ResidencePermitTypeAdmin)
admin.site.register(ResidencePermitStatus, ResidencePermitStatusAdmin)
admin.site.register(ResidencePermitProcess, ResidencePermitProcessAdmin)
admin.site.register(ProcessActivityType, ProcessActivityTypeAdmin)
admin.site.register(ResidencePermitProcessHistory, ResidencePermitProcessHistoryAdmin)
