from django.contrib.auth.models import User
from django.db import models


class ResidencePermitType(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return self.name


class ResidencePermitStatus(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return self.name


class ResidencePermitProcess(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    permit_type = models.ForeignKey(ResidencePermitType, on_delete=models.CASCADE,
                                    null=True)
    estimated_issue_date = models.DateField(null=True, blank=True)
    status = models.ForeignKey(ResidencePermitStatus, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class ProcessActivityType(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return self.name


class ResidencePermitProcessHistory(models.Model):
    residence_permit_process = models.ForeignKey(ResidencePermitProcess, on_delete=models.CASCADE)
    date = models.DateField()
    activity_type = models.ForeignKey(ProcessActivityType, on_delete=models.CASCADE)
    details = models.CharField(max_length=240)

    def __str__(self):
        return f"{self.date} - {self.activity_type}"