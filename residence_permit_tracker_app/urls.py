"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import *

app_name = "residence_permit_tracker_app"

urlpatterns = [
    path('process-history/', ResidencePermitProcessHistoryPage.as_view(),
         name='residence-permit-process-history-page'),
    path('single-process/<int:pk>', SinglePermitUpdateWidget.as_view(),
         name='single-residence-permit-process-form-widget'),
    path('process-history-record/create/', SinglePermitHistoryRecordCreatePage.as_view(),
         name='residence-permit-process-history-record-create-form-page'),
    path('process-history-record/<int:pk>/update/', SinglePermitHistoryRecordUpdatePage.as_view(),
         name='residence-permit-process-history-record-update-form-page'),
    path('process-history-record/<int:pk>/delete/', SinglePermitHistoryRecordDeleteView.as_view(),
         name='residence-permit-process-history-record-delete'),
]
