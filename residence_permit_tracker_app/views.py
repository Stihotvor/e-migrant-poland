from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView, ListView, CreateView, DeleteView

from residence_permit_tracker_app.forms import ResidencePermitProcessHistoryRecordCreateForm
from residence_permit_tracker_app.models import ResidencePermitProcess, ResidencePermitProcessHistory
from residence_permit_tracker_app.services.residence_permit_process import ResidencePermitProcessCase


class ResidencePermitProcessHistoryPage(LoginRequiredMixin, ListView):
    template_name = 'residence_permit_tracker_app/process_history_page.html'
    http_method_names = ['get']
    model = ResidencePermitProcess
    context_object_name = 'permits'

    def get_queryset(self, **kwargs):
        return ResidencePermitProcessCase.get_residence_permit_processes(user=self.request.user)


class SinglePermitUpdateWidget(LoginRequiredMixin, UpdateView):
    template_name = 'residence_permit_tracker_app/single_permit_widget.html'
    http_method_names = ['get', 'post']
    model = ResidencePermitProcess
    fields = ['permit_type', 'estimated_issue_date', 'status']

    def get_queryset(self, **kwargs):
        return ResidencePermitProcessCase.get_residence_permit_processes(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['permit_number'] = self.object.pk
        context['statuses'] = ResidencePermitProcessHistory.objects.filter(
            residence_permit_process=self.object).order_by('date')
        return context

    def get_success_url(self, **kwargs):
        return reverse_lazy("residence_permit_tracker:single-residence-permit-process-form-widget",
                            kwargs={'pk': int(self.kwargs.get('pk'))})


class SinglePermitHistoryRecordCreatePage(LoginRequiredMixin, CreateView):
    template_name = 'residence_permit_tracker_app/single_permit_history_record_create_page.html'
    http_method_names = ['get', 'post']
    form_class = ResidencePermitProcessHistoryRecordCreateForm

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super().get_initial()
        initial['residence_permit_process'] = ResidencePermitProcess.objects.filter(
            user=self.request.user,
        ).first()
        return initial

    def form_valid(self, form):
        # TODO: Rebuild the lookup for multiple processes
        form.residence_permit_process = ResidencePermitProcess.objects.filter(
            user=self.request.user,
        ).first()
        self.object = form.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse_lazy("residence_permit_tracker:residence-permit-process-history-page")


class SinglePermitHistoryRecordUpdatePage(LoginRequiredMixin, UpdateView):
    template_name = 'residence_permit_tracker_app/single_permit_history_record_update_page.html'
    http_method_names = ['get', 'post']
    model = ResidencePermitProcessHistory
    fields = ['date', 'activity_type', 'details']

    def get_queryset(self, **kwargs):
        return ResidencePermitProcessHistory.objects.filter(residence_permit_process__user=self.request.user)

    def get_success_url(self, **kwargs):
        return reverse_lazy("residence_permit_tracker:residence-permit-process-history-page")


class SinglePermitHistoryRecordDeleteView(LoginRequiredMixin, DeleteView):
    http_method_names = ['post']

    def get_queryset(self, **kwargs):
        return ResidencePermitProcessHistory.objects.filter(residence_permit_process__user=self.request.user)

    def get_success_url(self, **kwargs):
        return reverse_lazy("residence_permit_tracker:residence-permit-process-history-page")
