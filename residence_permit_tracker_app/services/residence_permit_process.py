import logging
from django.db import models
from django.db.models import QuerySet

from residence_permit_tracker_app.models import ResidencePermitProcess

log = logging.getLogger(f"project.{__name__}")


# TODO: Grab limited list of columns
class ResidencePermitProcessCase:
    @staticmethod
    def get_residence_permit_processes(user: models.Model) -> QuerySet:
        return ResidencePermitProcess.objects.filter(user=user)
