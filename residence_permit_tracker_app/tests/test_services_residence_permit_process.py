import pytest
from django.contrib.auth.models import User
from mixer.backend.django import mixer

from residence_permit_tracker_app.services.residence_permit_process import ResidencePermitProcessCase

pytestmark = pytest.mark.django_db


def test_get_resident_permit_processes():
    # Creating user
    user_obj = mixer.blend(User)

    # Generate permit types
    for _ in range(12):
        mixer.blend('residence_permit_tracker_app.ResidencePermitType')

    # Generate permit statuses
    for _ in range(12):
        mixer.blend('residence_permit_tracker_app.ResidencePermitStatus')

    # Creating residence permit processes
    for _ in range(12):
        mixer.blend('residence_permit_tracker_app.ResidencePermitProcess',
                    user=user_obj)

    # Checking whether documents are been created and getter is working properly
    assert len(ResidencePermitProcessCase.get_residence_permit_processes(user=user_obj)) == 12
