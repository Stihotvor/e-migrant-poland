"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from authapp.views import IndexPage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexPage.as_view(), name='index'),
    path('travel-documents/', include('travel_doc_tracker_app.urls', namespace='travel_doc_tracker')),
    path('border-crossing/', include('border_tracker_app.urls', namespace='border_crossing_tracker')),
    path('residence-permit/', include('residence_permit_tracker_app.urls', namespace='residence_permit_tracker')),
]
