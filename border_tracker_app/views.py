from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class BorderCrossingHistoryPage(LoginRequiredMixin, TemplateView):
    template_name = 'border_tracker_app/history_page.html'
