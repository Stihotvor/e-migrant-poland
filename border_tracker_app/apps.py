from django.apps import AppConfig


class BorderTrackerAppConfig(AppConfig):
    name = 'border_tracker_app'
