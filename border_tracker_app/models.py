from django.contrib.auth.models import User
from django.db import models


class TravelPermitType(models.Model):
    name = models.CharField(max_length=12, unique=True)


class TravelPermitLevel(models.Model):
    name = models.CharField(max_length=12, unique=True)


class TravelPermit(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    permit_type = models.ForeignKey(TravelPermitType, on_delete=models.CASCADE)
    issue_date = models.DateField()
    expiry_date = models.DateField()


class TransportType(models.Model):
    name = models.CharField(max_length=12, unique=True)


class PolishBorderCrossingHistory(models.Model):
    direction_choices = (
        ('enter', 'Entering'),
        ('leave', 'Leaving')
    )

    travel_permit = models.ForeignKey(TravelPermit, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    direction = models.CharField(max_length=10, choices=direction_choices)
    place = models.CharField(max_length=36)
    transport_type = models.ForeignKey(TransportType, on_delete=models.CASCADE)
    details = models.CharField(max_length=240)
