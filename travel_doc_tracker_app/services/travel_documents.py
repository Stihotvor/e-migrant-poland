import logging
from django.db import models
from django.db.models import QuerySet

from travel_doc_tracker_app.models import TravelDocument

log = logging.getLogger(f"project.{__name__}")


# TODO: Grab limited list of columns
class TravelDocumentCase:
    @staticmethod
    def __get_documents_count(user: models.Model) -> int:
        log.debug('Counting user travel documents')
        return int(TravelDocument.objects.filter(user=user).count())

    @staticmethod
    def get_documents(user: models.Model) -> QuerySet:
        log.debug('Fetching user travel documents')
        return TravelDocument.objects.filter(user=user).order_by('pk')

    @classmethod
    def check_documents(cls, user: models.Model):
        log.info('Verifying user documents presence')
        log.debug('Verifying user authentication')
        if user.is_authenticated:
            log.debug('User is authenticated')
            doc_count = cls.__get_documents_count(user=user)
            if doc_count < 3:
                log.warning('User has less than three documents! Creating documents.')
                docs_to_create = []
                for _ in range(3 - doc_count):
                    docs_to_create.append(TravelDocument(user=user))

                TravelDocument.objects.bulk_create(docs_to_create)
                log.debug('User travel documents have been created successfully')
