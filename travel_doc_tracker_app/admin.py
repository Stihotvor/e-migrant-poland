from django.contrib import admin

from travel_doc_tracker_app.models import TravelDocument


class TravelDocumentAdmin(admin.ModelAdmin):
    pass


admin.site.register(TravelDocument, TravelDocumentAdmin)
