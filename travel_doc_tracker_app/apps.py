from django.apps import AppConfig


class TravelDocTrackerAppConfig(AppConfig):
    name = 'travel_doc_tracker_app'
    verbose_name = 'Travel documents tracker'
