import pytest
from django.contrib.auth.models import User
from mixer.backend.django import mixer

from travel_doc_tracker_app.services.travel_documents import TravelDocumentCase
pytestmark = pytest.mark.django_db


def test_get_documents():
    # Creating user
    user_obj = mixer.blend(User)

    # Creating three documents
    for _ in range(3):
        mixer.blend('travel_doc_tracker_app.TravelDocument',
                    user=user_obj,
                    doc_type=mixer.blend('travel_doc_tracker_app.TravelDocumentType'))

    # Checking whether documents are been created and getter is working properly
    assert len(TravelDocumentCase.get_documents(user=user_obj)) == 3


def test_check_documents():
    # Creating
    user_obj = mixer.blend(User)

    # Checking whether documents not found
    assert len(TravelDocumentCase.get_documents(user=user_obj)) == 0

    # Checking whether document exist and if not - method will create ones
    TravelDocumentCase.check_documents(user=user_obj)

    # Checking whether documents have been created
    assert len(TravelDocumentCase.get_documents(user=user_obj)) == 3
