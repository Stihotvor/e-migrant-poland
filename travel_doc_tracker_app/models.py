from django.contrib.auth.models import User
from django.db import models


class TravelDocument(models.Model):
    doc_type_choices = (
        ('travel passport', 'Travel Passport'),
        ('other document', 'Other Document'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    doc_type = models.CharField(max_length=16, choices=doc_type_choices, null=True, blank=True)
    issue_date = models.DateField(null=True, blank=True)
    expiry_date = models.DateField(null=True, blank=True)

    def __str__(self):
        if self.doc_type:
            return str(self.doc_type)
        return str(self.user.username)
