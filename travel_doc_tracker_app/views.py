import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView, UpdateView

from travel_doc_tracker_app.models import TravelDocument
from travel_doc_tracker_app.services.travel_documents import TravelDocumentCase

log = logging.getLogger(f"project.{__name__}")


class DocumentTrackerPage(LoginRequiredMixin, TemplateView):
    template_name = 'travel_doc_tracker_app/document_tracker_page.html'
    http_method_names = ['get']

    def dispatch(self, *args, **kwargs):
        TravelDocumentCase.check_documents(user=self.request.user)
        return super().dispatch(*args, **kwargs)


class DocumentUpdateFormWidget(LoginRequiredMixin, UpdateView):
    template_name = 'travel_doc_tracker_app/single_document_widget.html'
    http_method_names = ['get', 'post']
    model = TravelDocument
    fields = ['doc_type', 'issue_date', 'expiry_date']
    # context_object_name = 'document'

    def get_object(self, queryset=None):
        log.debug(f'Requesting object no. {self.kwargs.get("pk")}')
        return TravelDocumentCase.get_documents(user=self.request.user)[int(self.kwargs.get('pk') - 1)]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['doc_number'] = int(self.kwargs.get('pk'))
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        if not self.object.doc_type:
            self.object.issue_date = None
            self.object.expiry_date = None

        if self.object.expiry_date and self.object.issue_date and \
            self.object.expiry_date <= self.object.issue_date:
            form.add_error('expiry_date', 'Should be more than issue date')
            return super().form_invalid(form)

        self.object = form.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse_lazy("travel_doc_tracker:single-document-update-form",
                            kwargs={'pk': int(self.kwargs.get('pk'))})
